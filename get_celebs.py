import cv2

import requests
from bs4 import BeautifulSoup
import urllib


import sys
import time

import os


def get_celebs(num_celebs=10):

    process_celebs = 0
    start = 0
    os.mkdir("images")
    haar_face_cascade = cv2.CascadeClassifier('data\haarcascade_frontalface_default.xml')

    while(1):
        num_on_step = min(num_celebs - process_celebs, 50)
        url = 'https://www.imdb.com/search/name?gender=male,female&start={0}&ref_=rlm'.format(start)
        response = requests.get(url)
        html_soup = BeautifulSoup(response.text, 'html.parser')

        celebs_img_containers = html_soup.find_all('div', class_='lister-item-image')
        for celeb in celebs_img_containers:
            # print(celeb.a.img['alt'])
            file_name = "{0}.jpg".format(celeb.a.img['alt'])
            urllib.request.urlretrieve(celeb.a.img['src'], "images/celebrity.jpg")
            try:
                # print(file_name)
                img = cv2.imread("images/celebrity.jpg", 1)
                gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                faces_haar = haar_face_cascade.detectMultiScale(gray, 1.1, 5)
                if len(faces_haar) == 0:
                    num_on_step -= 1
                    print("\tFace not detected for {0}".format(celeb.a.img['alt']))

                else:
                    (x, y, w, h) = faces_haar[0]
                    # img = cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
                    cv2.imwrite('output/{0}'.format(file_name), img[y:y+h, x:x+w])
                    process_celebs += 1
            except cv2.error as e:
                print("Skipped {0}".format(celeb.a.img['alt']))
                num_on_step -= 1

            os.remove("images/celebrity.jpg")
            if process_celebs >= num_celebs:
                break
        print("Processed {0} images of {1}".format(process_celebs, num_celebs))
        if process_celebs >= num_celebs:
            break
        start += 50
        time.sleep(1)

    os.rmdir("images")
    return

if __name__ == '__main__':
    if len(sys.argv) > 1:
        num = int(sys.argv[1])
        get_celebs(num_celebs=num)
    else:
        get_celebs()
